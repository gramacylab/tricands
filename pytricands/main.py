#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from scipy.spatial import Delaunay, ConvexHull
import numpy as np

def tricands_interior(X):
    """"
    interior, Delaunay triangulated candidates; subroutine
    used by tricands wrapper below

    X: input design matrix
    """
    
    m = X.shape[1]
    n = X.shape[0]
    if (n < m + 1):
        raise Exception("must have nrow(X) >= ncol(X) + 1")

    ## possible to further vectorize?
    ## find the middle of triangles
    tri = Delaunay(X, qhull_options = "Q12").simplices
    Xcand = np.zeros([tri.shape[0], m])
    for i in range(tri.shape[0]):
        Xcand[i, :] = np.mean(X[tri[i, ], ], axis = 0)

    return {'cand': Xcand, 'tri': tri}



def tricands_fringe(X, p = 0.5, lower = 0, upper = 1):
    """"
    fringe, outside convex hull candidates; subroutine
    used by tricands wrapper below; assumes a bounding box of [0, 1]^m
    
    X: input design matrix
    p: distance to the boundary (0 = on hull, 1 = on boundary)
    lower: lower bound of bounding box for all dimensions
    upper: upper bound of bounding box for all dimensions
    """

    m = X.shape[1]
    n = X.shape[0]
    if (n < m + 1):
        raise Exception("must have nrow(X) >= ncol(X) + 1")

    ## get midpoints of external (convex hull) facets and normal vectors
    qhull = ConvexHull(X, qhull_options = "n")
    norms = np.zeros((qhull.simplices.shape[0], m))
    Xbound = np.zeros((qhull.simplices.shape[0], m))
    for i in range(qhull.simplices.shape[0]): 
        Xbound[i, ] = np.mean(X[qhull.simplices[i, ], :], axis = 0)
        norms[i, ] = qhull.equations[i, 0:m] 

    ## norms off of the boundary points to get fringe candidates
    ## p specifies distance between hull and boundary
    eps = np.sqrt(np.finfo(float).eps)
    alpha = np.zeros(Xbound.shape[0])
    ai = np.zeros([Xbound.shape[0], m])
    pos = (norms > 0)
    ai[pos] = (upper - Xbound[pos])/norms[pos]
    ai[np.logical_not(pos)] = (lower - Xbound[np.logical_not(pos)])/norms[np.logical_not(pos)]
    ai[np.abs(norms) < eps] = np.Inf
    alpha = np.min(ai, axis = 1)

    Xfringe = Xbound + norms * alpha[:, np.newaxis] * p

    return {'XF': Xfringe, 'XB': Xbound, 'qhull': qhull}



def tricands(X, p = 0.5, fringe = True, nmax = None, best = None, 
             ordering = None, vis = False, imgname = 'tricands.pdf',
             lower = 0, upper = 1):
    """
    Triangulation Candidates for Bayesian Optimization
    (assumes a bounding box of [0, 1]^m)

    X: A numpy.array of size "n x m" with each row giving a design point and 
       each column a feature
    p: Distance to the boundary for fringe cands (0 = on hull, 1 = on boundary)
    fringe: A boolean variable giving whether to include fringe points to allow 
            exploration outside the convex hull of our points
    nmax: Maximum size of candidate set, employs strategic subsetting
    best: In Bayesian optimization, a (zero-indexed) integer giving the row which 
          corresponds to the best (lowest) currently observed point; only matters 
          if there is subsampling involved.
    ordering: In contour location, order (zero indexed) of closeness of rows of X to the
              contour level/limit (i.e. ordering = np.argsort(abs(y - limit))); only
              matters if there is subsampling involved.
    vis: A boolen telling us if you want a visualization of your triangulation; only 
         applicable to 2D designs.
    imgname: file name for saved plot (if vis == TRUE)
    lower: lower bound of bounding box for all dimensions
    upper: upper bound of bounding box for all dimensions

    Returns a numpy array which is like X.
    """
    ## extract dimsions and do sanity checks
    m = X.shape[1]
    n = X.shape[0]
    if nmax is None:
        nmax = 100 * m
    if vis and m != 2:
        raise Exception("visuals only possible when ncol(X) = 2")
    if n < m + 1:
        raise Exception("must have nrow(X) >= ncol(X) + 1")
    if best is not None and ordering is not None: 
        raise Exception("can only subset for BO or CL, not both")
    if np.min(X) < lower or np.max(X) > upper:
        raise Exception("X outside of lower/upper bounds")

    ## possible visual
    if vis:
        fig = plt.figure()
        plt.scatter(X[:, 0], X[:, 1])
        plt.xlim(lower, upper)
        plt.ylim(lower, upper)

    ## interior candidates
    ic = tricands_interior(X)
    Xcand = ic['cand']
    if vis:
        for i in range(ic['tri'].shape[0]): 
            X[np.append(ic['tri'][i, :], ic['tri'][i, 0]),].T
            for j in range(ic['tri'].shape[1] + 1):
                if j < ic['tri'].shape[1]:
                    xpoints = X[ic['tri'][i, j:(j + 2)], 0]
                    ypoints = X[ic['tri'][i, j:(j + 2)], 1]
                else:
                    xpoints = X[ic['tri'][i, [-1, 0]], 0]
                    ypoints = X[ic['tri'][i, [-1, 0]], 1]
                plt.plot(xpoints, ypoints, color = 'black')

        plt.scatter(Xcand[:, 0], Xcand[:, 1])
        
    ## calculate midpoints of convex hull vectors
    if fringe:
        fr = tricands_fringe(X, p, lower = lower, upper = upper)
        Xcand = np.concatenate([Xcand, fr['XF']], axis = 0)
        ## possibly visualize fringe candidates
        if vis: 
            for i in range(fr['XB'].shape[0]):
                plt.arrow(fr['XB'][i, 0], fr['XB'][i, 1], 
                          fr['XF'][i, 0] - fr['XB'][i, 0], 
                          fr['XF'][i, 1] - fr['XB'][i, 1], width = 0.005, color = 'red')

    ## throw some away?
    if nmax < Xcand.shape[0]:
        
        adj = []

        ## Bayesian optimization strategic subsetting
        if best is not None:
            ## find candidates adjacent to best
            adj = np.where(np.apply_along_axis(lambda x: np.any(x == best), 1, ic['tri']))[0]
            if len(adj) > nmax/10:
                sample = np.random.choice(adj, round(nmax/10), replace=False)
            if vis:
                plt.scatter(X[best:(best + 1), 0], X[best:(best + 1), 1], color = 'green')
            if len(adj) >= nmax:
                raise Exception("adjacent to best >= nmax")
            
        ## Contour location strategic subsetting
        if ordering is not None:
            i = 0
            facets = np.column_stack((fr['qhull'].simplices, [None] * fr['qhull'].simplices.shape[0]))
            all_tri = np.row_stack((ic['tri'], facets))
            while len(adj) < nmax / 10: 
                ## get all triangles adjacent to the i'th best point
                adj_tri = np.where(np.apply_along_axis(lambda x: np.any(x == ordering[i]), 1, all_tri))[0]
                if len(adj_tri) == 0 or i == len(ordering) - 1:
                    break
                else:
                    ## remove duplicates
                    duplicates = [i in adj for i in adj_tri]
                    adj_tri = adj_tri[np.logical_not(duplicates)]
                    adj.append(np.random.choice(adj_tri))
                    i = i + 1
            ## need to add optional visual

        ## get the rest randomly
        remain = np.array(list(range(Xcand.shape[0])))
        if len(adj) > 0:
            remain = np.delete(remain, adj, 0)
        rest = np.random.choice(remain, (nmax - len(adj)), replace = False)
        sel = np.concatenate([adj, rest], axis = 0).astype(int)
        Xcand = Xcand[sel, :]

        ## possibly visualize
        if vis:
            plt.scatter(Xcand[:, 0], Xcand[:, 1], color = "green")

    if vis:
        plt.savefig(imgname)
        plt.close()
    return Xcand
