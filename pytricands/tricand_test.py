#!/usr/bin/env python3
# -*- coding: utf-8 -*-

exec(open('main.py').read())

import matplotlib.pyplot as plt
from scipy.spatial import Delaunay, ConvexHull
import numpy as np

## test tricands
X = np.random.uniform(size=[40,2])
#X = np.concatenate([X, np.stack([[0,0],[1,1],[0,1],[1,0]])])

Xtri = tricands(X, vis = True)
