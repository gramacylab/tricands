# Tricands - Triangulation candidates for Bayesian Optimization

Tricands is an alternative to random, or LHS/space-filling generation of candidates for Bayesian Optimization and other active learning tasks.  It can also be an effective replacement for derivitive-based optimization of BO acquisition functions.  

## Overview

The code is authored by Robert B. Gramacy, Nathan Wycoff, and Annie Sauer.  Links to our technical report describing the methodology are forthcoming.  Scripts supporting the Monte Carlo benchmarking experiments therin are provided in the "R" directory.

## Implementation

We provide an implementation in both R and python.  These are based on existing wrappers around the Qhull C library for convex hulls and Delauney triangulations via the "quickhull" algorithm.  For details, see the paper linked above.

### R 

Simply source the file "tricands.R" in the "R" directory. See R/tricand\_test.R for example usage.  Other files in this directory support the Monte Carlo benchmarking exercises in our paper, linked above.

### python

This directory is setup as a module using python wheels. It is recommended to use a virtual environment to install this package. 

Using pip, simply do:

terminal> python3 -m pip install git+https://bitbucket.org/gramacylab/tricands.git

Then to get started, in python:

```python
import numpy as np
from pytricands import tricands

X = np.random.uniform(size=[10,2])
cands = tricands(X)
print(cands)
```

## License

Both Python and R libraries are licensed under [LGPL](https://www.gnu.org/licenses/lgpl-3.0.en.html).