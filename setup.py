from setuptools import setup, find_packages

setup(
    name="tricands",
    version="0.0.1",
    packages=find_packages(),
    install_requires=["numpy>=1.19.5", "scipy>=1.5.4", "matplotlib>=3.3.4","wheel"],
)
